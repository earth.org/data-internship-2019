Made a series of images of HK LANDSAT map form 1986-2019. Also to a video.

Resolution: 30metres/pixel

Images produced in a batch, to be improved:
* LANDSAT 7 line scan failure
* Edges
* 1972-1986 data only available in Red/Green bands, not included here

Exported images and video: [Google Drive](https://drive.google.com/open?id=1xZz3DStcS7bjAfBhqh4TLvA586i9uZ_U)
