# Google Earth Engine time series with Savitzky-Golay filter

Extract image collection values for a feature collection, create a vetetaion index time series data frame and apply a Savitzky-Golay filter over it.
Savitzky Golay filter simply put is a digital filter that could be quite useful for smoothing of image based data.
(https://en.wikipedia.org/wiki/Savitzky%E2%80%93Golay_filter)